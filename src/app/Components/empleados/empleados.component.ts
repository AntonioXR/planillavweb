import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SalarioService } from "../../Services/salario.service";
import { IgssService } from "../../Services/igss.services";

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html'
})
export class EmpleadosComponent implements OnInit {

  constructor(private _router:Router, private _salario:SalarioService ,private _igss:IgssService) { }
  listIgss:Array<any> = []
  listSalarios: Array<any> = []
  ngOnInit() {
    console.log(this._router.url);
    this._igss.getIgss(res=>{
      this.listIgss = res.splice(0, 3)
    })
    this._salario.getSalarios(res=>{
      this.listSalarios = res.splice(0, 3)
    })
  }
  
 
}
