import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { EmpleadoService } from "../../../Services/empleado.services";
import { MdDialog } from "@angular/material";
import { DialogsService,ListDService } from "../../../Services/DiagEliminar.services";
import { MdSnackBar } from "@angular/material";
import { BonificacionService } from "../../../Services/bonificacion.services";
import { ISRService } from "../../../Services/isr.services";

@Component({
    selector: 'app-empledos-ag',
    template: `
    <div id='lista-empleados'>
        <md-card *ngFor='let emp of empleadosList' style='margin: 10px;'>
        <i class="material-icons" style='float: right;' data-toggle="tooltip" title="Eliminar salario" (click)='eliminar(emp.nombre+" "+emp.apellido, emp.idEmpleado)'>delete</i>
        <md-chip-list style='float: right;'>
            <md-chip color="{{(emp.idEstado == 1? 'primary' : 'accent')}}"  selected="true">{{emp.estado}}</md-chip>
        </md-chip-list>
            <md-card-title>{{emp.nombre+' '+emp.apellido}}</md-card-title>
            <md-card-content>
                <p>Fecha de inicio: {{emp.fecha_i_format}}</p>
                <p>{{ (emp.fecha_f_format != null)? 'Fecha de finalizacion:'+ emp.fecha_f_format : '' }}</p>
            </md-card-content>
                <small class11="form-text text-muted" [routerLink]="['editar',emp.idEmpleado]">Presione para ver detalles y modificar</small>
        </md-card>
    </div>
    `
})

export class EmpleadosLSComponent implements OnInit {
    empleadosList:Array<any> = [] 

    constructor(private _router:Router,private _snack:MdSnackBar,private _list:ListDService,private _empleado:EmpleadoService,
        private _empleados:EmpleadoService, private _eliminar:DialogsService,private _bonificacion:BonificacionService,
        private  _isr:ISRService) { }

    ngOnInit() {
        this.cargarDatos()
     }

     cargarDatos(){
        this._empleado.getEmpleados(localStorage.getItem('UDI'), res=>{
            this.empleadosList = res
        })
     }

     eliminar(nombre_emp:string, ID){
        let diag = this._eliminar.confirm('Esta seguro?','Desea eliminar el empleado '+nombre_emp+'?').subscribe(res=>{
          if(res == true){
              this._empleado.remove(ID, res=>{
                  this.cargarDatos()
                this._snack.open(res.Mensaje, 'Cerrar', {duration: 2300})
              })
          }          
        })
    
      }

}