import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AdminService {
    uri:string = 'http://localhost:3000/api/admin/';
    constructor(private http: Http) { }
    
    login(data, callback){        
        this.http.post(this.uri+'log', data ).toPromise()
        .then(res=>{            
           callback(res.json())
        })
        .catch(res=>{
            console.log(res);
            callback({Mensaje: 'Hubo un error en la red', Result: false})
         })
    }

    registrar(data, callback){
        this.http.post(this.uri, data).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
             console.log(res);
             callback({Mensaje: 'Hubo un error en la red', Result: false})
          })
    }

    perfil(data, callback){
        this.http.put(this.uri+data.idAdministrador, data).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
             console.log(res);
             callback({Mensaje: 'Hubo un error en la red', Result: false})
          })
    }
}