import { Component, OnInit } from '@angular/core';
import { PlanillaService } from "../../../Services/planilla.services";
import { MdSnackBar } from "@angular/material";
import { DialogsService } from "../../../Services/DiagEliminar.services";


@Component({
    selector: 'app-planilla-list',
    template: `
    
    
    <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
    <a class="nav-link active" (click)='ngOnInit()' data-toggle="tab" href="#general" role="tab">Todas</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#mes" role="tab">Mes</a>
    </li>
    </ul>
    
    <div class="tab-content">
    <div class="tab-pane active" id="general" role="tabpanel">
    <md-card style='margin: 10px;'  *ngFor='let data of listaPlanillas'>
    <div class='row'>
        <div class='col-11'>
            <md-card-title>Planilla de {{monthNames[data.mes-1]}} de {{data.anio}}</md-card-title>
            <md-card-subtitle>Planilla generada: {{data.fecha_f}}</md-card-subtitle>      
            <md-card-content>
            <h4>Total: Q{{data.total_planilla}}</h4>
            <h5>Comentarios: </h5> <p>{{ (data.comentario != null) ? data.comentario :'Ningun comentario' }}</p>
                <p>No. detalles {{data.total_detalle}}</p>
            </md-card-content>
        </div>

        <div class='col-1 button-row'>
        <button md-icon-button>
            <md-icon class="md-24" color="primary" [routerLink]="['editar', data.idPlanilla]" routerLinkActive="active" >mode_edit</md-icon>
        </button>
        <button md-icon-button (click)='eliminar(data.idPlanilla, data.mes -1, data.anio)'>
            <md-icon class="md-24" color="accent">delete</md-icon>
        </button>
        </div>
    </div>
    </md-card>
    </div>
    <div class="tab-pane mt-4" id="mes" role="tabpanel">

    <md-select class='mb-3' placeholder="Año" (change)='buscarAnio()' [(ngModel)]='datosAnio' >
        <md-option *ngFor="let ani of anio" [value]="ani.anio">
        {{ ani.anio }}
        </md-option>
    </md-select>
  

        <md-card style='margin: 10px;'  *ngFor='let data of listaPlanillas'>
        <div class='row'>
            <div class='col-11'>
                <md-card-title>Planilla de {{monthNames[data.mes-1]}} de {{data.anio}}</md-card-title>
                <md-card-subtitle>Planilla generada: {{data.fecha_f}}</md-card-subtitle>      
                <md-card-content>
                <h4>Total: Q{{data.total_planilla}}</h4>
                <h5>Comentarios: </h5> <p>{{ (data.comentario != null) ? data.comentario :'Ningun comentario' }}</p>
                    <p>No. detalles {{data.total_detalle}}</p>
                </md-card-content>
            </div>

            <div class='col-1 button-row'>
            <button md-icon-button>
                <md-icon class="md-24" color="primary" [routerLink]="['editar', data.idPlanilla]" routerLinkActive="active" >mode_edit</md-icon>
            </button>
            <button md-icon-button (click)='eliminar(data.idPlanilla, data.mes -1, data.anio)'>
                <md-icon class="md-24" color="accent">delete</md-icon>
            </button>
            </div>
        </div>
        </md-card>
    </div>
    </div>
    
    
    
  
    `
})

export class PlanillaListComponent implements OnInit {
   
    anio = []
monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
  ]
  datosAnio = ''

  listaPlanillas:Array<any> = []
    constructor(private _snack:MdSnackBar, private _diag:DialogsService,private _planilla:PlanillaService) { }

    ngOnInit() {
        this._planilla.getPlanillas(res=>{
            this.listaPlanillas = res
        })
        this._planilla.anio().subscribe(res=>{
            this.anio = res
            console.log(this.anio);
            
        })
     }
     eliminar(ID, mes, anio){
         let diag  = this._diag.confirm('Esta seguro?', 'eliminara la planilla de '+this.monthNames[mes]+' de '+anio)
         diag.subscribe(res=>{
             if(res == true){
                 this._planilla.delete(ID, res=>{
                     this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                     this.ngOnInit()
                 })
             }
         })
     }
     buscarAnio(){
        this._planilla.getByYear(this.datosAnio).subscribe(res=>{
            this.listaPlanillas = res
        })
     }
}