import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SalarioService {
  constructor(private http: Http) { }
  URI = 'http://localhost:3000/api/salario/'
  getSalarios(callback){
    this.http.get(this.URI).toPromise()
    .then(res=>{
        callback(res.json())
    })
    .catch(res=>{
        callback([])
        console.log(res);
    })
  }
  getS(ID){
    return this.http.get(this.URI+ID).map(res=> res.json())
  }
  getFecha(fecha, callback){
      this.http.get(this.URI+'anio/'+fecha).toPromise()
      .then(res=>{
        callback(res.json())
      })
      .catch(res=>{
        callback({Mensaje: 'Hubo un error de red'})
        console.log(res);
        
      })
  }
  insert(data, callback){
    this.http.post(this.URI, data).toPromise()
    .then(res=>{
        callback(res.json())
    })
    .catch(res=>{
        callback({Mensaje: 'Error de red', Result: false})
        console.log(res);
    })
  }

  update(data, callback){
    this.http.put(this.URI+data.idSalario, data).toPromise()
    .then(res=>{
        callback(res.json())
    })
    .catch(res=>{
        callback({Mensaje: 'Error de red', Result: false})
        console.log(res);
    })
  }
  delete(ID, callback){
    this.http.delete(this.URI+ID).toPromise()
    .then(res=>{
        callback(res.json())
    })
    .catch(res=>{
        callback({Mensaje: 'Error de red', Result: false})
        console.log(res);
    })
  }

}