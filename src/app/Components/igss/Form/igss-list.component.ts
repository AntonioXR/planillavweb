import { MdSnackBar } from "@angular/material";
import { Component, OnInit } from '@angular/core';
import { IgssService } from "../../../Services/igss.services";
import { Router } from "@angular/router";
import { DialogsService } from "../../../Services/DiagEliminar.services";

@Component({
    selector: 'app-igss-list',
    template: `
     <md-card style='margin: 10px;' *ngFor='let igss of listaIGSS'>
        <md-card-title style='float: left'>
            Cuota de IGSS: Q{{igss.cuota}}
        </md-card-title>
        <i class="material-icons"  style="float: rigth ;margin-left: 10px" (click)='eliminar(igss.cuota, igss.anio, igss.idIGSS)'>delete</i>
        <md-card-subtitle style='text-align: end'>
            Año: {{igss.anio}}
        </md-card-subtitle>
        <small [routerLink]="['editar',igss.idIGSS]">Has click aca para modificar</small>
    </md-card>
    `
})

export class IgssListComponent implements OnInit {
    listaIGSS:Array<any> = []
    constructor(private _igss:IgssService, private _router:Router, private _snack:MdSnackBar, private _diag:DialogsService  ) { }

    ngOnInit() {
        this.cargarDatos()
     }

    cargarDatos(){
        this._igss.getIgss(res=>{
            this.listaIGSS = res
        })
    }

    eliminar(couta, anio, ID){
        let dialogo = this._diag.confirm('Desea eliminar la cuota para ', anio+' con cuota de Q'+couta+'?')
        dialogo.subscribe(res=>{
            if(res == true){
                this._igss.delete(ID, res=>{
                    this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                    this.cargarDatos()
                })
            }
        })
    }


}