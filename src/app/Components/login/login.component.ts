import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { AdminService } from "../../Services/administrador.services";
import {MdSnackBar} from '@angular/material';
import { CookieService } from "ngx-cookie";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  showBar = false
  constructor(private _router:Router,private _admin:AdminService, private snack:MdSnackBar, private _cookie:CookieService) { }
  ngOnInit() {
    if(localStorage.getItem('UDI') != null && localStorage.getItem('UNA')!= null){
      this._router.navigateByUrl('empleados')
    }else{
      this._router.navigateByUrl('')
    }
  }

  login(data:NgForm){
    this.showBar= true
    if(data.valid){
        this._admin.login({nick: data.value.nick, contrasena: data.value.pass}, res=>{
          if(res.Result){
            this._cookie.put('UDI',res.idAdministrador)
            localStorage.setItem('UDI',res.idAdministrador)
            localStorage.setItem('UNA',res.nombre)
            localStorage.setItem('USD', JSON.stringify(res))
            this._router.navigateByUrl('empleados')
          }else{
            this.snack.open(res.Mensaje, 'Cerrar',{duration: 2500})
            this.showBar= false
          }
        })
    }else{
      this.snack.open('HEY! faltan datos', 'Cerrar',{duration: 2500})
      this.showBar= false
    }
  }

}
