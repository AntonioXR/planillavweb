import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PlanillaService {
    URI = 'http://localhost:3000/api/planilla/'
    constructor(private http: Http) { }
    getPlanillas(callback){
        this.http.get(this.URI+'adm/'+localStorage.getItem('UDI')).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }
    getPreview(data, callback){
        this.http.get(this.URI+'adm/'+localStorage.getItem('UDI')+'/'+data.mes+'/'+data.anio).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }
    getByMonth(data, callback){
        this.http.get(this.URI+`${data.mes}/${data.anio}`).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }
    getP(ID){
        return this.http.get(this.URI+ID.toString()).map(res=> res.json())
    }
    getByYear(year){
        console.log(year);
        
        return this.http.get(this.URI+localStorage.getItem('UDI')+'/anio/'+year).map(res=> res.json())
    }
    insert(data, callback){
        this.http.post(this.URI, data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }

    anio(){
       return this.http.get(this.URI+'adm/'+localStorage.getItem('UDI')+'/anio').map(res=> res.json())
    }

    update(data, callback){
        this.http.put(this.URI+data.idPlanilla, data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }
    delete(ID, callback){
        this.http.delete(this.URI+ID).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
        })
    }
}