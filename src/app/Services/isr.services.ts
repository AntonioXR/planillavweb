import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ISRService {
    URI = 'http://localhost:3000/api/isr/' 
    constructor(private http: Http) { }
    getISRs(ID, callback){
        this.http.get(this.URI+`em/${ID}`).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
            
        })
    }
    getISR(ID){
       return this.http.get(this.URI+ID).map(res=> res.json())
    }

    insert(data,callback){
        this.http.post(this.URI, data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
            
        })
    }
    delete(ID, callback){
        this.http.delete(this.URI+ID).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
            
        })
    }
}