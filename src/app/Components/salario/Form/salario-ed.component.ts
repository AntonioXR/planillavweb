import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { SalarioService } from "../../../Services/salario.service";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { MdSnackBar } from "@angular/material";

@Component({
    selector: 'app-salario-ag',
    template: `
    <h3>Editar</h3>
    <div class='container'>
        <form>
            <div class='row'>
                <img style='width: 20%; height: 20%' src='./assets/salario-icon.jpg' />
                <div class='col-lg-5 col-md-5 col-sm-11'>
                    <div class='form-group'>
                        <label for='salario-ag'>Salario</label>
                        <input type='number' step='0.001' id='salario-ag' class='form-control' name='couta' [(ngModel)]='data.cuota' />
                    </div>

                    <div class='form-group'>
                        <md-select placeholder="Año" disabled='true'  [(ngModel)]="data.anio" name='anio'>
                            <md-option *ngFor="let anio of anios" [value]="anio">
                            {{anio}}
                            </md-option>
                        </md-select>
                    </div>
                    <small>{{mensaje}}</small>

                    <button type='submit' class='btn btn-outline-warning' (click)='editar()' >Guardar cambios</button>
                    <button type='submit' class='btn btn-outline-secundary' (click)='this._router.navigateByUrl("salario")' >Volver</button>
                </div>
            </div>
        </form>
    </div>    

    `
})

export class SalarioEditarComponent implements OnInit {
    data:any = {
        "anio": new Date().getFullYear().toString(),
        "cuota": 0.0,
        idSalario: 0
    }
    mensaje = ''
    constructor(private _active:ActivatedRoute, private _salario:SalarioService, private _router:Router, private _snack:MdSnackBar) {
        this._active.paramMap.switchMap((param:ParamMap)=> this.data = this._salario.getS(param.get('id'))).subscribe(res=>{
            this.data= res[0]
            this.data.anio = res[0].anio.toString()
            this.mensaje = 'Fecha de creacion: '+res[0].fecha_f
        })
        
     }
    anios=['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
    ngOnInit() { }

    editar(){
        if(this.data.cuota != 0.0 && this.data.anio != ''){

            this._salario.update(this.data, res=>{
                this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                setTimeout(()=>{
                    this._router.navigateByUrl('salario')
                },2000)
            })
        }else{
            this._snack.open('Faltan datos', 'Cerrar', {duration: 2400})
        }
    }

}