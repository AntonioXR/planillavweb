//Modulos base
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { CookieModule } from 'ngx-cookie';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { RouterAPP } from "./app.router";
//Componentes de inicio
import { AppComponent } from './app.component';
import { NavBarComponent } from './Components/nav-bar/nav-bar.component';
import { LoginComponent } from './Components/login/login.component';
//Servicios de conexion a WS
import { AdminService } from "./Services/administrador.services";
import { EmpleadoService } from "./Services/empleado.services";
import { DialogsService,ListDService } from "./Services/DiagEliminar.services";
import { BonificacionService } from './Services/bonificacion.services'
import { ISRService } from "./Services/isr.services";
import { SalarioService } from "./Services/salario.service";
import { IgssService } from "./Services/igss.services";
import { PlanillaService } from "./Services/planilla.services";
import { DetallePService } from "./Services/detalleP.services";
//Componentes para Empleado
import { EmpleadosComponent } from './Components/empleados/empleados.component';
import { EmpleadosLSComponent } from './Components/empleados/Form/empleados-list';
import { EmpleadosAGComponent } from './Components/empleados/Form/empleados-agregar';
import { EmpleadoEDComponent } from "./Components/empleados/Form/empleado-editar";
//Componentes para salario
import { SalarioComponent } from './Components/salario/salario.component';
import { SalarioListComponent } from "./Components/salario/Form/salario-list.component";
import { SalarioAgregarComponent } from "./Components/salario/Form/salario-ag.component";
import { SalarioEditarComponent } from "./Components/salario/Form/salario-ed.component";
//Componentes para IGSS
import { IgssComponent } from './Components/igss/igss.component';
import { IgssListComponent } from "./Components/igss/Form/igss-list.component";
import { IgssAgregarComponent } from "./Components/igss/Form/igss-ag.component";
import { IgssEditarComponent } from "./Components/igss/Form/igss-ed.component";
//Modulos y servicios de complemento
import {MdSnackBarModule, MdDialogModule, MdDatepickerModule,MdListModule,MdSelectModule,MdButtonModule,MdIconModule,
  MdProgressBarModule, MdCardModule, MdChipsModule, MdSlideToggleModule, MdProgressSpinnerModule} from '@angular/material';
import { EliminarDiagComponent } from './Components/eliminar-diag/eliminar-diag.component';
import { DataListComponent } from './Components/eliminar-diag/listado-datos.component';

import { PlanillaComponent } from './Components/planilla/planilla.component';
import { PlanillaAgregarComponent } from "./Components/planilla/Actions/planilla-generar.component";
import { PlanillaListComponent } from "./Components/planilla/Actions/planilla-list.component";
import { PlanillaEditarComponent } from './Components/planilla/Actions/planilla-editar/planilla-editar.component';
import { DetallePEditarComponent } from "./Components/planilla/Actions/detalle-planilla-editar.component";


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    DetallePEditarComponent,
    LoginComponent,
    EmpleadosAGComponent,
    IgssAgregarComponent,
    DataListComponent,
    IgssListComponent,
    EmpleadosComponent,
    EmpleadosLSComponent,
    PlanillaAgregarComponent,
    PlanillaListComponent,
    EliminarDiagComponent,
    SalarioEditarComponent,
    EmpleadoEDComponent,
    IgssEditarComponent,
    SalarioComponent,
    SalarioAgregarComponent,
    SalarioListComponent,
    IgssComponent,
    PlanillaComponent,
    PlanillaEditarComponent
  ],
  imports: [
    BrowserModule,  
    RouterAPP,
    MdIconModule,
    MdButtonModule,
    MdSelectModule,
    MdDatepickerModule,
    MdCardModule,
    MdSnackBarModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdSlideToggleModule,
    MdProgressBarModule,
    MdChipsModule, 
    MdListModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    FormsModule,
    HttpModule
  ],
  providers: [
    AdminService,
    EmpleadoService,
    ListDService,
    DialogsService,
    BonificacionService,
    ISRService,
    SalarioService,
    DetallePService,
    PlanillaService,
    IgssService
  ],
  entryComponents:[
    EliminarDiagComponent,
    DataListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
