import { Component, OnInit } from '@angular/core';
import { SalarioService } from "../../../Services/salario.service";
import { Router } from "@angular/router";
import { MdSnackBar } from "@angular/material";

@Component({
    selector: 'app-salario-ag',
    template: `
    <h3>Agregar</h3>
    <div class='container'>
        <form>
            <div class='row'>
                <img style='width: 20%; height: 20%' src='./assets/salario-icon.jpg' />
                <div class='col-lg-5 col-md-5 col-sm-11'>
                    <div class='form-group'>
                        <label for='salario-ag'>Salario</label>
                        <input type='number' step='0.001' id='salario-ag' class='form-control' name='couta' [(ngModel)]='data.cuota' />
                    </div>

                    <div class='form-group'>
                        <md-select placeholder="Año" [(ngModel)]="data.anio" name='anio' (change)='obtener()'>
                            <md-option *ngFor="let anio of anios" [value]="anio">
                            {{anio}}
                            </md-option>
                        </md-select>
                    </div>
                    <small>{{mensaje}}</small>

                    <button type='submit' class='btn btn-outline-primary' [disabled]='activo' (click)='agregar()' >Agregar nuevo salario</button>
                    <button type='submit' class='btn btn-outline-secundary' (click)='this._router.navigateByUrl("salario")' >Volver</button>
                </div>
            </div>
        </form>
    </div>    

    `
})

export class SalarioAgregarComponent implements OnInit {
    data = {
        "anio": new Date().getFullYear().toString(),
        "cuota": 0.0
    }
    activo = true;
    mensaje = ''
    constructor(private _salario:SalarioService, private _router:Router, private _snack:MdSnackBar) {
        
     }
    anios=['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
    ngOnInit() { }

    agregar(){
        if(this.data.cuota != 0.0 && this.data.anio != ''){

            this._salario.insert(this.data, res=>{
                this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                setTimeout(()=>{
                    this._router.navigateByUrl('salario')
                },2000)
            })
        }else{
            this._snack.open('Faltan datos', 'Cerrar', {duration: 2400})
        }
    }
    obtener(){
        this._salario.getFecha(this.data.anio, res=>{
            if(res.length == 0){
                this.activo = false
                this.mensaje = ''
            }else{
                this.mensaje = 'Ya existe un salario para este año'
                this.data.cuota = res[0].cuota
                this.data.anio = res[0].anio.toString()
                this.activo = true
            }   
        })
    }
}