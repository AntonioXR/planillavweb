import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from "@angular/material";
import { DialogsService } from "../../../../Services/DiagEliminar.services";
import { PlanillaService } from "../../../../Services/planilla.services";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { DetallePService } from "../../../../Services/detalleP.services";

@Component({
  selector: 'app-planilla-editar',
  templateUrl: './planilla-editar.component.html'
})
export class PlanillaEditarComponent implements OnInit {
    monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
  ];

 anios=['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']

 data = {
     "idPlanilla": 0,
     "fecha_creacion": "",
     "mes": 0,
     "anio": 0,
     "comentario": '',
     "idAdministrador": localStorage.getItem('UDI'),
     "total_planilla":0,
     "total_bonificacion":0,
     "total_isr":0,
     "total_sueldo_o":0,
     "total_salario_l":0,
     "total_salario_b":0,
     "total_igss":0
 }

 listaDatos:Array<any>= []

 constructor(private _active:ActivatedRoute,private _router:Router,private _snack:MdSnackBar,private _detalleP:DetallePService,private _planilla:PlanillaService) { }

 ngOnInit() {
    this._active.paramMap.switchMap((params:ParamMap)=> this._planilla.getP(params.get('id'))).subscribe(res=>{
      this.data = res[0]
      this._detalleP.getDetalles(this.data.idPlanilla, res=>{
          this.listaDatos = res
      })
      this._detalleP.getTotal(this.data.idPlanilla).subscribe(res=>{
          this.data.total_planilla = res[0].total_planilla 
          this.data.total_bonificacion = res[0].total_bonificacion
          this.data.total_isr = res[0].total_isr
          this.data.total_sueldo_o = res[0].total_sueldo_o
          this.data.total_salario_l = res[0].total_salario_l
          this.data.total_salario_b = res[0].total_salario_b
          this.data.total_igss = res[0].total_igss
      })
    })
  }

  guardar(ID){
      localStorage.setItem('IDP', ID)      
  }

  editar(){
      this._planilla.update(this.data, res=>{
          this._snack.open(res.Mensaje, 'Cerrar', {duration: 3000})
          setTimeout(()=> {
              this._router.navigateByUrl('planilla')
          }, 2700);
      })
  }
}
