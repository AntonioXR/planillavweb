import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from "@angular/material";
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { DetallePService } from '../../../Services/detalleP.services'

@Component({
    selector: 'app-detallePlanilla-editar',
    templateUrl: 'detalle-planilla-editar.component.html'
})

export class DetallePEditarComponent implements OnInit {
    constructor(private _snack:MdSnackBar, private _router:Router, private _active:ActivatedRoute, private _detalle:DetallePService) { }
    datos =  {
        "idDetalleP": 0,
        "idEmpleado": 0,
        "nombre_empleado": "",
        "estado_empleado": 0,
        "idPlanilla": 0,
        "bonificacion": 0,
        "retencion_isr": 0,
        "sueldo_ordinario": 0,
        "salario_liquido": 0,
        "salario_base": 0,
        "igss": 0,
        "fecha_inicio": ""
    }
    ngOnInit() {
        this._active.paramMap.switchMap((param:ParamMap)=> this._detalle.getD(param.get('id'))).subscribe(res=>{
            this.datos = res[0]
            console.log(this.datos, 'Ingreso');
            
        })
     }

     actualizar(){
         this.datos.salario_base = Number((this.datos.sueldo_ordinario + this.datos.bonificacion + this.datos.retencion_isr).toFixed(2))
         this.datos.salario_liquido = Number((this.datos.salario_base - this.datos.retencion_isr- this.datos.igss).toFixed(2))
     }

     editar(){
         if(this.datos.bonificacion !- 0 && this.datos.retencion_isr != 0 && this.datos.igss != 0 ){
             this._detalle.update(this.datos, res=>{
                 this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                 var IDP = localStorage.getItem('IDP')
                 localStorage.removeItem('IDP')
                 setTimeout(()=>{this._router.navigateByUrl('planilla/editar/'+IDP)}, 2000)
                })
            }else{
                this._snack.open('Faltan datos', 'Cerrar', {duration: 2400})
            }
    }    
}