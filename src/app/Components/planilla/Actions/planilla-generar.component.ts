import { Component, OnInit } from '@angular/core';
import { PlanillaService } from "../../../Services/planilla.services";
import { DetallePService } from "../../../Services/detalleP.services";
import { MdSnackBar } from "@angular/material";
import { Router } from "@angular/router";

@Component({
    selector: 'app-planilla-add',
    templateUrl: `./planilla-generar.component.html`
})

export class PlanillaAgregarComponent implements OnInit {
    carga = false
    gene = false
    comenta = false
    no = 0
    mensaje = ''
    monthNames = [
       {mes: "Enero", numero: 1}, {mes:"Febrero", numero: 2}, {mes:"Marzo", numero: 3},{mes: "Abril", numero: 4},{mes: "Mayo", numero: 5},{mes: "Junio", numero: 6},
    {mes:"Julio", numero: 7}, {mes:"Agosto", numero: 8},{mes: "Septiembre", numero: 9},{mes: "Octubre", numero: 10}, {mes:"Noviembre", numero: 11},{mes: "Diciembre", numero: 12}];
  anios=['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
    data = {
        "idPlanilla": 0,
        "fecha_creacion": "",
        "mes": 0,
        "anio": 0,
        "comentario": '',
        "idAdministrador": localStorage.getItem('UDI')
    }
    listaDatos:Array<any>= []
    constructor(private _router:Router,private _snack:MdSnackBar,private _detalleP:DetallePService,private _planilla:PlanillaService) { }

    ngOnInit() { }
    buscar(){
        this.carga = true
        this._planilla.getByMonth(this.data, res=>{
            if(res.length == 0){
                this.comenta = false
                this._planilla.getPreview(this.data, resp=>{
                    this.listaDatos = resp
                    this.carga = false
                    this.mensaje = ''
                    this.gene = true
                    this.data.comentario = ''
                })
            }else{
                this.comenta = true
                this._detalleP.getDetalles(res[0].idPlanilla, resp=>{
                    this.listaDatos = resp
                    this.gene = false
                    this.mensaje = 'Ya existe una planilla con estos datos'
                    this.carga = false
                    this.data.comentario = res[0].comentario
                })      
            }
        })
    }
    generar(){
        this._planilla.insert(this.data, res=>{
            this._snack.open(res.Mensaje, 'Cerrar', {duration: 3000})
            setTimeout(()=> {
                this._router.navigateByUrl('planilla')
            }, 2700);
        })
    }
}