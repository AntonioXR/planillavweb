import { Component, OnInit } from '@angular/core';
import {  MdSnackBar } from "@angular/material";
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { EmpleadoService } from "../../../Services/empleado.services";
import { MdDialog } from "@angular/material";
import { DialogsService,ListDService } from "../../../Services/DiagEliminar.services";
import { BonificacionService } from "../../../Services/bonificacion.services";
import { ISRService } from "../../../Services/isr.services";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';


@Component({
    selector: 'app-editar-em',
    template: `
    <h3>Editar</h3>
    <div class='container'>
        <form  >
            <div class='row'>
                <img style='width: 19%; height: 19%' src='./assets/user-icon.png' />
                <div class='col-lg-5 col-md-5 col-sm-11'>
                    <div class='form-group'>
                        <label for='nombre-ag'>Nombre</label>
                        <input id='nombre-ag' required class='form-control' name='nombre-ag' [(ngModel)]='empleadoEdit.nombre' />
                    </div>

                    <div class='form-group'>
                        <label for='apellido-ag'>Apellido</label>
                        <input id='apelldo-ag' required class='form-control' name='apelldo-ag' [(ngModel)]='empleadoEdit.apellido' />
                    </div>

                    <div class='form-group'>
                        <label for='estado-ag'>Estado</label>
                        <md-slide-toggle name='fecha-ini-ag' required [checked]='empleadoEdit.estado_e' (change)='estado()'></md-slide-toggle>
                    </div>
                    <div class='form-group'>
                        <label for='fecha-ini-ag'>Fecha de inicio</label>
                        <input type='date' id='fecha-ini-ag' required class='form-control' name='fecha-ini-ag' [(ngModel)]='empleadoEdit.fecha_i_format' />
                    </div>
                </div>

                <div class='col-lg-4 col-md-5 col-sm-11'>
                    <div class='form-group' *ngIf='!empleadoEdit.estado_e'>
                        <label for='fecha-fin-ag'>Fecha de Finalizacion</label>
                        <input type='date' id='fecha-fin-ag' required class='form-control' name='fecha-fin-ag' [(ngModel)]='empleadoEdit.fecha_f_format' />
                    </div>
                    <label for='bonificacion-ag'>Bonificacion</label> 
                    <div class='form-group form-inline'>
                        <h5 style='margin-right: 10px'> Q{{empleadoEdit.bonificacion}}    </h5> 
                        <i data-toggle="tooltip" title="Editar las bonificaciones" class="material-icons" (click)='mostrar("bonificacion",IDE )'>mode_edit</i>
                    </div>
                    <label for='isr-ag'>ISR</label>
                    <div class='form-group form-inline'>
                        <h5 style='margin-right: 10px'> Q{{empleadoEdit.isr}} </h5> 
                        <i class="material-icons" (click)='mostrar("isr",IDE )' data-toggle="tooltip" title="Editar el ISR">mode_edit</i>
                    </div>
                    <button type='submit' class='btn btn-outline-warning' (click)='editar()' >Guardar Cambos</button>
                    <button type='submit' class='btn btn-outline-secundary' (click)='volver()' >Volver</button>
                </div>
            </div>
        </form>
    </div>    
    `
})

export class EmpleadoEDComponent implements OnInit {
    empleadoEdit = {
        "idEmpleado":0,
        "nombre": "",
        "apellido": "",
        "fecha_inicio": "",
        "fecha_inactividad": "",
        "idEstado": 1,
        "idAdministrador": localStorage.getItem('UDI'),
        "estado_e": false ,
        fecha_i_format: '',
        fecha_f_format: '',
        "bonificacion": 0.0,
        "isr": 0.0,
        fecha_f: ''
    }
    estado(){
        this.empleadoEdit.idEstado = (this.empleadoEdit.idEstado == 2)? 1 :2
        this.empleadoEdit.estado_e =  (this.empleadoEdit.idEstado == 1)?true :false
    }
    IDE='' ;

    constructor(private _router:Router, private _empleado:EmpleadoService, private _activate:ActivatedRoute,
        private _snake:MdSnackBar,private _snack:MdSnackBar,private _list:ListDService, private _eliminar:DialogsService,
        private _bonificacion:BonificacionService,
        private  _isr:ISRService) {
     }

    cargarDatos(){
        this._activate.paramMap.switchMap((param:ParamMap)=>{this.IDE = param.get('id');return this._empleado.getE(param.get('id'))}).subscribe(res=>{
            this.empleadoEdit = res[0]
            this.empleadoEdit.estado_e =  (this.empleadoEdit.idEstado == 1)?true :false
        })
    }

    ngOnInit() {
        this.cargarDatos()
     }

    volver(){
        this._router.navigateByUrl('empleados')
    }

    editar(){
        if(this.empleadoEdit.nombre != '' && this.empleadoEdit.apellido  != '' && this.empleadoEdit.fecha_inicio  != ''){
            console.log(this.empleadoEdit);
            
            this.empleadoEdit.fecha_inicio = this.empleadoEdit.fecha_i_format
            this.empleadoEdit.fecha_inactividad = this.empleadoEdit.fecha_f_format
            console.log(this.empleadoEdit);
            
            this._empleado.update(this.empleadoEdit, resp=>{
                this._snake.open(resp.Mensaje, 'Cerrar', {duration: 2500})
                setTimeout(()=>{
                    this._router.navigateByUrl('empleados')
                }, 800)
            })
        }else{
            this._snake.open('Faltan datos!', 'Cerrar', {duration: 2500})
        }
    }

    mostrar(type:string, IDE){
        if(type == 'isr'){

           this._isr.getISRs(IDE, res=>{
               
               let diag = this._list.confirm('ISR',res, IDE)
               diag.afterClosed().subscribe((respo) =>{
                console.log(respo);
                   if(respo.idISR != null || respo.idISR != undefined ){
                   //Esta eliminando un ISR
                   this._isr.delete(respo.idISR, re=>{
                       this._snack.open(re.Mensaje, 'Cerrar', {duration: 2400})
                       this.cargarDatos()
                   })
                   }else if(respo != false){
                   //Esta agregando un ISR
                   this._isr.insert(respo, re=>{
                       this._snack.open(re.Mensaje, 'Cerrar', {duration: 2400})
                       this.cargarDatos()
                   })
                   }
               })
           })

        }else if (type != 'isr'){

           this._bonificacion.getBonificaciones(IDE, res=>{
               
               let diag = this._list.confirm('Bonificacion',res, IDE)

               diag.afterClosed().subscribe((respo) =>{
                   console.log(respo);
                   
                   if(respo.idBonificacion != null || respo.idBonificacion != undefined ){
                   //Esta eliminando una Bonificacion
                   this._bonificacion.delete(respo.idBonificacion, re=>{
                       this._snack.open(re.Mensaje, 'Cerrar', {duration: 2400})
                       this.cargarDatos()
                   })
                   }else if(respo != false){
                   //Esta agregando una Bonificacion
                   this._bonificacion.insert(respo, re=>{
                       this._snack.open(re.Mensaje, 'Cerrar', {duration: 2400})
                       this.cargarDatos()
                   })
                   }
               })
           })
           
        }
     }

}