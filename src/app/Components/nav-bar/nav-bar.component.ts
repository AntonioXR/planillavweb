import { Component, OnInit } from '@angular/core';
import { CookieService } from "ngx-cookie";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styles: []
})
export class NavBarComponent implements OnInit {

  constructor(private _cookie:CookieService) { }

  ngOnInit() {}

  LOG():boolean{
    if(localStorage.getItem('UDI') != null){
      return true;    
    }else{
      return false;
    }
  }
  cerrar(){
    localStorage.clear()
  }

}
