import { MdSnackBar } from "@angular/material";
import { Component, OnInit } from '@angular/core';
import { SalarioService } from "../../../Services/salario.service";
import { Router } from "@angular/router";
import { DialogsService } from "../../../Services/DiagEliminar.services";

@Component({
    selector: 'app-list-salario',
    template: `
    <md-card style='margin: 10px;' *ngFor='let sala of listaSalarios'>
        <md-card-title style='float: left'>
            Salario base: Q{{sala.cuota}}
        </md-card-title>
        <i class="material-icons"  style="float: rigth ;margin-left: 10px" (click)='eliminar(sala.cuota, sala.anio, sala.idSalario)'>delete</i>
        <md-card-subtitle style='text-align: end'>
            Año: {{sala.anio}}
        </md-card-subtitle>
        <small [routerLink]="['editar',sala.idSalario]">Has click aca para modificar</small>
    </md-card>
    `
})

export class SalarioListComponent implements OnInit {
    listaSalarios:Array<any> = []

    constructor(private _snack:MdSnackBar ,private _router:Router,private _salario:SalarioService, private _diag:DialogsService) { }

    ngOnInit() { 
        this.cargarDatos()
    }
    
    cargarDatos(){
        this._salario.getSalarios(res=>{
            this.listaSalarios = res
        })
    }
    eliminar(couta, anio, ID){
        let dialogo = this._diag.confirm('Desea eliminar el salario para ', anio+' con Salario de Q'+couta+'?')
        dialogo.subscribe(res=>{
            if(res == true){
                this._salario.delete(ID, res=>{
                    this._snack.open(res.Mensaje, 'Cerrar', {duration: 2400})
                    this.cargarDatos()
                })
            }
        })
    }

}