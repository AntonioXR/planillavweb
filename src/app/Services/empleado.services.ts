import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class EmpleadoService {

    uri:string = 'http://localhost:3000/api/empleado/';

    constructor(private http: Http) { }
    
    getEmpleados(ID, callback){
        this.http.get(this.uri+'ad/'+ID).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback({Mensaje: 'Error de Red!', Result: false})
            console.log(res);
            
        })
    }
    getE(ID){
        return this.http.get(this.uri+ID).map(res=> res.json())
    }
    insert(data, callback){
        this.http.post(this.uri,data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback({Mensaje: 'Error de Red!', Result: false})
            console.log(res);
            
        })
    }
    update(data, callback){
        this.http.put(this.uri+data.idEmpleado ,data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback({Mensaje: 'Error de Red!', Result: false})
            console.log(res);
            
        })
    }
    remove(ID, callback){
        this.http.delete(this.uri+ID).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback({Mensaje: 'Error de Red!', Result: false})
            console.log(res);
            
        })
    }
}