import { Observable } from 'rxjs/Rx';
import { EliminarDiagComponent } from '../Components/eliminar-diag/eliminar-diag.component'
import { DataListComponent } from '../Components/eliminar-diag/listado-datos.component'
import { MdDialogRef, MdDialog, MdDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable()
export class DialogsService {

    constructor(private dialog: MdDialog) { }

    public confirm(title: string, message: string): Observable<any> {

        let dialogRef: MdDialogRef<EliminarDiagComponent>;

        dialogRef = this.dialog.open(EliminarDiagComponent);
        dialogRef.componentInstance.title = title;
        dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }
}


@Injectable()
export class ListDService {

    constructor(private dialog: MdDialog) { }

    public confirm(title: string, list: Array<any>, idEmpleado){

        let diag: MdDialogRef<DataListComponent>;

        diag = this.dialog.open(DataListComponent)
        
        diag.componentInstance.title = title
        diag.componentInstance.lista = list
        diag.componentInstance.data.idEmpleado = idEmpleado

        return diag
    }
}