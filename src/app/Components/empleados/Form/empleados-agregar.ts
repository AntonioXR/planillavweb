import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from "@angular/material";
import { Router } from "@angular/router";
import { EmpleadoService } from "../../../Services/empleado.services";

@Component({
    selector: 'app-empleado-ag',
    template: `
    <h3>Agregar</h3>
    <div class='container'>
        <form >
            <div class='row'>
                <img style='width: 19%; height: 19%' src='./assets/user-icon.png' />
                <div class='col-lg-5 col-md-5 col-sm-11'>
                    <div class='form-group'>
                        <label for='nombre-ag'>Nombre</label>
                        <input id='nombre-ag' required class='form-control' name='nombre-ag' [(ngModel)]='data.nombre' />
                    </div>

                    <div class='form-group'>
                        <label for='apellido-ag'>Apellido</label>
                        <input id='apelldo-ag' required class='form-control' name='apelldo-ag' [(ngModel)]='data.apellido' />
                    </div>

                    <div class='form-group'>
                        <label for='estado-ag'>Estado</label>
                        <md-slide-toggle name='fecha-ini-ag' required [(checked)]='data.estado_e' (change)='estado()'></md-slide-toggle>
                    </div>
                    <div class='form-group'>
                        <label for='fecha-ini-ag'>Fecha de inicio</label>
                        <input type='date' id='fecha-ini-ag' required class='form-control' name='fecha-ini-ag' [(ngModel)]='data.fecha_inicio' />
                    </div>
                </div>

                <div class='col-lg-4 col-md-5 col-sm-11'>
                    <div class='form-group' *ngIf='!data.estado_e'>
                        <label for='fecha-fin-ag'>Fecha de Finalizacion</label>
                        <input type='date' id='fecha-fin-ag' required class='form-control' name='fecha-ini-ag' [(ngModel)]='data.fecha_inactividad' />
                    </div>
                    <div class='form-group'>
                        <label for='bonificacion-ag'>Bonificacion</label>
                        <input type='number' step='0.001' id='bonificacion-ag' required class='form-control' name='bonificacion-ag' [(ngModel)]='data.bonificacion' />
                    </div>
                    <div class='form-group'>
                        <label for='isr-ag'>ISR</label>
                        <input type='number' step='0.001' id='isr-ag' class='form-control' required name='isr-ag' [(ngModel)]='data.isr' />
                    </div>
                    <button type='submit' class='btn btn-outline-primary' (click)='agregar()' >Crear empleado</button>
                    <button type='submit' class='btn btn-outline-secundary' (click)='volver()' >Volver</button>
                </div>
            </div>
        </form>
    </div>    
    `
})

export class EmpleadosAGComponent implements OnInit {
    
    data = {
        nombre: '',
        apellido: '',
        estado: 1,
        fecha_inicio: '',
        fecha_inactividad: '',
        idAdministrador: localStorage.getItem('UDI'),
        bonificacion: 0.0,
        isr: 0.0,
        estado_e: true
    }

    estado(){
        this.data.estado_e = !this.data.estado_e 
        this.data.estado = (this.data.estado_e)?1 :2
    }
    
    constructor(private _snake:MdSnackBar,private _empleado:EmpleadoService, private _router:Router) { 

    }

    agregar(){
        if(this.data.nombre != '' && this.data.apellido  != '' && this.data.fecha_inicio  != ''){

            this._empleado.insert(this.data, resp=>{
                this._snake.open(resp.Mensaje, 'Cerrar', {duration: 2500})
                setTimeout(()=>{
                    this._router.navigateByUrl('empleados')
                }, 800)
            })
        }else{
            this._snake.open('Faltan datos!', 'Cerrar', {duration: 2500})
        }
    }
    volver(){
        this._router.navigateByUrl('empleados')
    }
    ngOnInit() { }
}