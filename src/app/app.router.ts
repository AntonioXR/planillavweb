import { Routes, RouterModule  } from "@angular/router";
import { LoginComponent } from "./Components/login/login.component";
import { EmpleadosComponent } from "./Components/empleados/empleados.component";
import { EmpleadosLSComponent } from "./Components/empleados/Form/empleados-list";
import { EmpleadosAGComponent } from "./Components/empleados/Form/empleados-agregar";
import { EmpleadoEDComponent } from "./Components/empleados/Form/empleado-editar";

import { SalarioComponent } from "./Components/salario/salario.component";
import { SalarioListComponent } from "./Components/salario/Form/salario-list.component";
import { SalarioAgregarComponent } from "./Components/salario/Form/salario-ag.component";
import { SalarioEditarComponent } from "./Components/salario/Form/salario-ed.component";

import { IgssComponent } from "./Components/igss/igss.component";
import { IgssListComponent } from "./Components/igss/Form/igss-list.component";
import { IgssAgregarComponent } from "./Components/igss/Form/igss-ag.component";
import { IgssEditarComponent } from "./Components/igss/Form/igss-ed.component";

import { PlanillaComponent } from "./Components/planilla/planilla.component";
import { PlanillaAgregarComponent } from "./Components/planilla/Actions/planilla-generar.component";
import { PlanillaEditarComponent } from "./Components/planilla/Actions/planilla-editar/planilla-editar.component";
import { PlanillaListComponent } from "./Components/planilla/Actions/planilla-list.component";
import { DetallePEditarComponent } from "./Components/planilla/Actions/detalle-planilla-editar.component";

const APP_ROUTER:Routes = [ 
    {path:'' , component:LoginComponent},
    {path:'empleados' , component:EmpleadosComponent, children: [
        {path:'', component:EmpleadosLSComponent},
        {path:'agregar', component:EmpleadosAGComponent},
        {path: 'editar/:id', component:EmpleadoEDComponent}
    ]},
    {path: 'salario', component: SalarioComponent, children: [
        {path: '', component: SalarioListComponent},
        {path: 'agregar', component: SalarioAgregarComponent},
        {path: 'editar/:id', component: SalarioEditarComponent}
    ]},
    {path: 'igss', component: IgssComponent, children: [
        {path: '', component: IgssListComponent},
        {path: 'agregar', component: IgssAgregarComponent},
        {path: 'editar/:id', component: IgssEditarComponent}
        
    ]},
    {path: 'planilla', component: PlanillaComponent,children: [
        {path:'', component: PlanillaListComponent},
        {path:'generar', component: PlanillaAgregarComponent},
        { path: 'editar/:id', component:PlanillaEditarComponent },
        {path: 'detalle/:id', component: DetallePEditarComponent}
    ]}
]

export const RouterAPP = RouterModule.forRoot(APP_ROUTER);