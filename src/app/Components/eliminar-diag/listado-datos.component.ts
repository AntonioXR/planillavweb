import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-list-data',
  template: `
  <h2>{{title}}</h2>
  <div>
    <div style='height: 25%'>
    <md-list>
      <md-list-item *ngFor='let datos of lista'>Cuota: Q{{datos.cuota}} Fecha: {{datos.fecha_f}}  Mes: {{monthNames[datos.mes - 1 ].mes}}  Año: {{datos.anio}}<i class="material-icons" (click)='diag.close(datos)'>delete_forever</i></md-list-item>
   </md-list>
    </div>
    <div class='form-group form-inline' style='height: 7%'>
      <button class='btn' (click)='diag.close(false)' style='padding: 3px; margin: 2px;'><i class="material-icons">clear</i></button>
      
      <input type='number'  [(ngModel)]='data.cuota' class='form-control' placeholder='Cuota' style='width: 8vw' style='margin: 2px;' />


      <md-select placeholder="Mes" [(ngModel)]="data.mes" name="mes">
      <md-option *ngFor="let mes of monthNames" [value]="mes.numero">
        {{mes.mes}}
      </md-option>
    </md-select>

    <md-select placeholder="Año" [(ngModel)]="data.anio" name="anio">
    <md-option *ngFor="let anio of anios" [value]="anio">
      {{anio}}
    </md-option>
  </md-select>


      <button class='btn btn-success' (click)='diag.close(data)' style='padding: 3px; margin: 2px;'><i class="material-icons">add</i></button>
    </div>
  </div>
  `
})

export class DataListComponent implements OnInit {
  constructor(public diag: MdDialogRef<DataListComponent>) { }

  ngOnInit() { }

  title:string = ''

  lista:Array<any> = []

  data:any = {
    idEmpleado : '',
    cuota: '',
    mes: '',
    anio: ''
  }

  monthNames = [
    {mes: "Enero", numero: 1}, {mes:"Febrero", numero: 2}, {mes:"Marzo", numero: 3},{mes: "Abril", numero: 4},{mes: "Mayo", numero: 5},{mes: "Junio", numero: 6},
 {mes:"Julio", numero: 7}, {mes:"Agosto", numero: 8},{mes: "Septiembre", numero: 9},{mes: "Octubre", numero: 10}, {mes:"Noviembre", numero: 11},{mes: "Diciembre", numero: 12}];
anios=['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']


}