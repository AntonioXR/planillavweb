
import { MdDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'confirm-dialog',
    template: `
        <p>{{ title }}</p>
        <p>{{ message }}</p>
        
        <button type="button" md-button md-dialog-close class='btn btn-outline-secundary' 
        (click)="dialogRef.close(true)">OK</button>

        <button type="button" md-button [md-dialog-close]="true" class='btn btn-outline-secundary' 
        (click)="dialogRef.close()">Cancel</button>
    `,
})
export class EliminarDiagComponent {

    public title: string='';
    public message: string='';

    constructor(public dialogRef: MdDialogRef<EliminarDiagComponent>) {

    }
}
