import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DetallePService {
    URI = 'http://localhost:3000/api/detalleP/'
    constructor(private http: Http) { }
    getDetalles(ID, callback){
        this.http.get(this.URI+'pla/'+ID).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback([])
            console.log(res);
            
        })
    }
    getD(ID){
       return this.http.get(this.URI+ID).map(res => res.json())
    }

    getTotal(ID){
        return this.http.get(this.URI+'pla/total/'+ID).map(res => res.json())
     }

    
    update(data, callback){
        this.http.put(this.URI+data.idDetalleP, data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            callback({Mensaje: 'Hubo un error de red'})
            console.log(res);
            
        })
    }
    delete(ID, callback){
        this.http.delete(this.URI+ID).toPromise()
        .then(res=>{
            callback(res)
        })
        .catch(res=>{
            callback({Mensaje: 'Hubo un error de red'})
            console.log(res);
        })
    }
}